#pragma once

#include <utils/macro.hpp>

#ifndef UTUMNO_DEBUG
#define UTUMNO_ENABLE_ASSERT
#endif

#ifdef UTUMNO_ENABLE_ASSERT
#include <cassert>
#include <source_location>

void UTUMNO_ASSERT(
    bool condition, const std::string_view message,
    const std::source_location location = std::source_location::current()) {
  if (!condition) {
    std::cerr << "Assertion failed(file: " << location.file_name()
              << ", line: " << location.line()
              << ", function: " << location.function_name() << "): " << message
              << '\n';
    std::terminate();
  }
}
#else

#define UTUMNO_ASSERT(cond, msg) (void)

#endif