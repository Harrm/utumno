#pragma once

#define UNIQUE_NAME(base) __FILE__##__LINE__##base

#ifndef NDEBUG
#   define UTUMNO_DEBUG
#else
#   ifdef UTUMNO_DEBUG
#       undef UTUMNO_DEBUG
#   endif
#endif
