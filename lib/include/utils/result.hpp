#pragma once

#include <system_error>
#include <variant>

#include <utils/macro.hpp>

namespace utumno {

template <typename T>
concept Error = true;

template <typename T, Error E>
class [[nodiscard("This class is intended for error processing")]] Result {
 public:
  using NoValueType = std::monostate;
  static constexpr bool HasValue = !std::is_same_v<T, void>;
  using ValueType = std::conditional_t<HasValue, T, NoValueType>;
  using ErrorType = E;

  static_assert(!std::is_same_v<ErrorType, void>,
                "Result cannot have a void Error type");

  template <bool _HasValue = HasValue, typename = std::enable_if_t<!_HasValue>>
  Result() {}

  template <typename T_>
  Result(T_ && t) : content{std::forward<T_>(t)} {}

  template <Error E_>
  Result(E_ && e) : content{std::forward<E_>(e)} {}

  template <bool _HasValue = HasValue>
  std::enable_if_t<_HasValue, ValueType const&> value() const& {
    return std::get<ValueType>(content);
  }

  template <bool _HasValue = HasValue>
  std::enable_if_t<_HasValue, ValueType&&> value() && {
#ifdef UTUMNO_DEBUG
    if(!std::holds_alternative<ValueType>(content)) throw std::runtime_error("Attempt to obtain a value from a result which holds an error");
#endif
    return std::get<ValueType>(std::move(content));
  }

  template <bool _HasValue = HasValue>
  std::enable_if_t<!_HasValue, void> value() const {
#ifdef UTUMNO_DEBUG
    if(!std::holds_alternative<ValueType>(content)) throw std::runtime_error("Attempt to obtain a value from a result which holds an error");
#endif
  }

  ErrorType const& error() const { 
#ifdef UTUMNO_DEBUG
    if(!std::holds_alternative<ErrorType>(content)) throw std::runtime_error("Attempt to obtain a value from a result which holds an error");
#endif
  return std::get<ErrorType>(content); 
  }

 private:
  std::variant<ValueType, ErrorType> content;
};

#define TRY_RESULT(value, expr) auto&& value =

}  // namespace utumno