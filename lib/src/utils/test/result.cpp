#include <gtest/gtest.h>

#include <utils/result.hpp>

using utumno::Result;

TEST(ResultTest, VoidValueResultHasDefaultConstructor) {
  Result<void, int> res{};
}
