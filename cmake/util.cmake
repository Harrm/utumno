
function(utumno_add_library lib_name)
    add_library(${lib_name} ${ARGN})
    if(${ARGV1} STREQUAL "INTERFACE")
        set(include_type "INTERFACE")
    else()
        set(include_type "PUBLIC")
    endif()
    target_include_directories(${lib_name} ${include_type} ${PROJECT_SOURCE_DIR}/include)
endfunction()
