#include <injector/injector.hpp>
#include <application/application.hpp>

int main(int argc, char** argv, char** env) {
    auto injector = utumno::InjectionContainer::create(argc, argv, env).value();
    auto app = injector->injectApplication();
    app->run();
    return 0;
}