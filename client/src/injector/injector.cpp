#include "injector.hpp"

#include <application/impl/app_configurator.hpp>
#include <application/impl/sdl_application.hpp>

namespace utumno {

Result<std::unique_ptr<InjectionContainer>, InjectorError>
InjectionContainer::create(int argc, char** argv, char** env) {
  auto configurator = std::make_unique<CompositeAppConfigurator>();
  std::unique_ptr<InjectionContainer> injector{new InjectionContainer{
      std::unique_ptr<AppConfigurator>(configurator.release())}};
  return injector;
}

std::unique_ptr<Application> InjectionContainer::injectApplication() const {
  return SdlApplication::create(configurator->read_config(argc, argv, env))
      .value();
}

}  // namespace utumno
