#pragma once

#include <application/application_config.hpp>
#include <utils/result.hpp>

namespace utumno {

class Application;

struct InjectorError {};

class InjectionContainer {
 public:

  static Result<std::unique_ptr<InjectionContainer>, InjectorError> create(
      int argc, char** argv, char** env);

  std::unique_ptr<Application> injectApplication() const;

 private:
  InjectionContainer(std::unique_ptr<AppConfigurator> configurator)
      : configurator{std::move(configurator)} {}

  std::unique_ptr<AppConfigurator> configurator;
  int argc;
  char** argv;
  char** env;
};

}  // namespace utumno