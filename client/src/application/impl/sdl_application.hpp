#pragma once

#include <application/application.hpp>

#include <memory>

#include <application/application_config.hpp>
#include <application/types.hpp>

struct SDL_Window;
struct SDL_Surface;

namespace utumno {

class SdlApplication : public Application {
 public:
  static Result<std::unique_ptr<SdlApplication>, ApplicationError> create(
      std::unique_ptr<ApplicationConfig> config);

  virtual void run() override;

 private:
  SdlApplication(std::unique_ptr<ApplicationConfig> config);

  static constexpr WindowSize DEFAULT_WINDOW_SIZE{640, 480};
  SDL_Surface* surface;
  SDL_Window* window;
  std::unique_ptr<ApplicationConfig> config;
};

}  // namespace utumno