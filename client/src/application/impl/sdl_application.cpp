#include "sdl_application.hpp"

#include <SDL.h>
#include <fmt/format.h>

namespace utumno {

Result<std::unique_ptr<SdlApplication>, ApplicationError>
SdlApplication::create(std::unique_ptr<ApplicationConfig> config) {
  std::unique_ptr<SdlApplication> app{new SdlApplication{std::move(config)}};
  auto win_size = app->config->get_window_size().value_or(DEFAULT_WINDOW_SIZE);

  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    return ApplicationError{
        fmt::format("SDL could not initialize: {}\n", SDL_GetError())};
  }
  app->window =
      SDL_CreateWindow("Utumno", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                       win_size.width, win_size.height, SDL_WINDOW_SHOWN);
  if (app->window == nullptr) {
    return ApplicationError{
        fmt::format("Window could not be created: {}\n", SDL_GetError())};
  }

  app->surface = SDL_GetWindowSurface(app->window);

  return std::move(app);
}

void SdlApplication::run() {
  SDL_FillRect(surface, nullptr, SDL_MapRGB(surface->format, 0xFF, 0xFF, 0xFF));
  SDL_UpdateWindowSurface(window);
  SDL_Event e;
  bool quit = false;
  while (!quit) {
    while (SDL_PollEvent(&e)) {
      if (e.type == SDL_QUIT) {
        quit = true;
      }
      if (e.type == SDL_KEYDOWN) {
        quit = true;
      }
      if (e.type == SDL_MOUSEBUTTONDOWN) {
        quit = true;
      }
    }
  }

  SDL_DestroyWindow(window);

  SDL_Quit();
}

SdlApplication::SdlApplication(std::unique_ptr<ApplicationConfig> config)
    : config{std::move(config)} {}

}  // namespace utumno
