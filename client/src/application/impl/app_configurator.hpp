#pragma once

#include <application/application_config.hpp>

#include <ranges>
#include <vector>

namespace utumno {

class UtumnoConfig : public ApplicationConfig {
 public:
  virtual void enhanceWith(const ApplicationConfig& other) override {}

  virtual std::optional<WindowSize> get_window_size() const override { return std::nullopt; }
};

class CliAppConfigurator : public AppConfigurator {
 public:
  virtual std::unique_ptr<ApplicationConfig> read_config(int argc, char** argv,
                                                         char** env) override {}

 private:
};

class ConfigFileAppConfigurator : public AppConfigurator {
 public:
  virtual std::unique_ptr<ApplicationConfig> read_config(int argc, char** argv,
                                                         char** env) override {}

 private:
};

class EnvAppConfigurator : public AppConfigurator {
 public:
  virtual std::unique_ptr<ApplicationConfig> read_config(int argc, char** argv,
                                                         char** env) override {}

 private:
};

class CompositeAppConfigurator : public AppConfigurator {
 public:
  CompositeAppConfigurator() = default;

  void addConfigurator(std::unique_ptr<AppConfigurator> configurator) {
      configurators.push_back(std::move(configurator));
  }

  virtual std::unique_ptr<ApplicationConfig> read_config(int argc, char** argv,
                                                         char** env) override {
    if (configurators.empty()) return std::make_unique<UtumnoConfig>();
    auto config = configurators.front()->read_config(argc, argv, env);
    for (auto& configurator : std::ranges::views::drop(configurators, 1)) {
      config->enhanceWith(*configurator->read_config(argc, argv, env));
    }
    return config;
  }

 private:
  std::vector<std::unique_ptr<AppConfigurator>> configurators;
};

}  // namespace utumno