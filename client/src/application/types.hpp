#pragma once

#include <cstdint>

namespace utumno {

    struct WindowSize {
        uint32_t width, height;
    };

}