#pragma once

#include <utils/result.hpp>

namespace utumno {

struct ApplicationError {
  std::string msg;
};

class Application {
 public:
  virtual ~Application() = default;

  virtual void run() = 0;
};

}  // namespace utumno
