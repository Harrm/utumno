#pragma once

#include <optional>
#include <memory>

#include <application/types.hpp>

namespace utumno {

class ApplicationConfig {
 public:
  virtual ~ApplicationConfig() = default;

  /**
   * @brief Fills uninitialized fields with corresponding fields present in \param other
   *
   * @param other Some other config
   */
  virtual void enhanceWith(const ApplicationConfig& other) = 0;

  virtual std::optional<WindowSize> get_window_size() const = 0;
};

class AppConfigurator {
 public:
  virtual ~AppConfigurator() = default;

  virtual std::unique_ptr<ApplicationConfig> read_config(int argc, char** argv,
                                                         char** env) = 0;
};

}  // namespace utumno