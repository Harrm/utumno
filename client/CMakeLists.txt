cmake_minimum_required(VERSION 3.10)

project(UtumnoClient CXX)

include(cmake/dependencies.cmake)
include(cmake/util.cmake)

add_subdirectory(src/application)
add_subdirectory(src/injector)

add_executable(utumno_client src/main.cpp)
target_link_libraries(utumno_client PRIVATE utumno_injector)
